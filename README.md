### User guide
Once git and git lfs are installed (see links below), **clone** the repository on your computer using the following bash lines:
```
cd directory/where/you/want/to/clone/the/repository
git clone https://thomasdobbelaere@bitbucket.org/thomasdobbelaere/bahamas_whitings.git
```

Then, to **update** the repository, use: 

```
git pull
```

if you encounter an error message using `git pull`, use:

```
git stash
```

then `git pull` again.

If you **modify** the documents of the repository, use the following lines once your modifications are done to update the repository online: 
   
```
git add --all
git commit -m "message explaining your modifications"
git push
```

In order to avoid conflicts between my modifications and your modifications, it is safer to `git pull` before you perform any modification that you might want to push.

### Useful links

* git: https://git-scm.com/
* git lfs: https://git-lfs.github.com/ 
* 7-zip: https://www.7-zip.org/

mp4 files are too heavy for bitbucket or github. Working on another solution...	
